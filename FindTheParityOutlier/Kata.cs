﻿using System;
using System.Linq;

namespace FindTheParityOutlier
{
    public class Kata
    {
        private static Func<int, bool> Predicate(bool isEven = true) => x => x % 2 == (isEven ? 0 : 1);

        public static int Find(int[] integers)
        {
            return integers != null && !integers.Count().Equals(0)
                ? integers.Single(Predicate(integers.Count(Predicate()).Equals(1)))
                : -1;
        }
    }
}
