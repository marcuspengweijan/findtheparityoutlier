﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FindTheParityOutlier.Tests
{
    [TestClass()]
    public class KataTests
    {
        [TestMethod()]
        public void FindTest_3()
        {
            int[] exampleTest = { 2, 6, 8, -10, 3 };
            Assert.IsTrue(3 == Kata.Find(exampleTest));
        }
        [TestMethod()]
        public void FindTest_206847684()
        {
            int[] exampleTest = { 206847684, 1056521, 7, 17, 1901, 21104421, 7, 1, 35521, 1, 7781 };
            Assert.IsTrue(206847684 == Kata.Find(exampleTest));
        }
        [TestMethod()]
        public void FindTest_0()
        {
            int[] exampleTest = { int.MaxValue, 0, 1 };
            Assert.IsTrue(0 == Kata.Find(exampleTest));
        }
        [TestMethod()]
        public void FindTest_Null()
        {
            int[] exampleTest = null;
            Assert.IsTrue(-1 == Kata.Find(exampleTest));
        }
        [TestMethod()]
        public void FindTest_Empty()
        {
            int[] exampleTest = new int[] { };
            Assert.IsTrue(-1 == Kata.Find(exampleTest));
        }
    }
}
